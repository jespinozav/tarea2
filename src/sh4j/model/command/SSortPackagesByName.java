package sh4j.model.command;

import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * sorts based on packages name.
 * @author Javier
 */
public class SSortPackagesByName extends SCommand {

  @Override
  public void executeOn(SProject project) {
    List<SPackage> temp = project.packages();
    Collections.sort(temp, new Comparator<SPackage>() {
      public int compare(SPackage sp1, SPackage sp2) {
        return sp1.toString().compareTo(sp2.toString());
      }
    });
  }

}
