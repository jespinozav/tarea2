package sh4j.model.command;


import sh4j.model.browser.SProject;

/**
 * It represent a command that could be applied to a project.
 * 
 * @author juampi
 *
 */
public abstract class SCommand {
  /**
   * sort an array list.
   * @param project based on sort
   * @return a sorted list based on something
   */
  public abstract void executeOn(SProject project);
  /**
   * returns a name class.
   * @return name of class
   */
  public String name() {
    return this.getClass().getName();
  }
}
