package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * object package.
 * @author Javier
 */
public class SPackage implements SObject {
  /**
   * string name.
   */
  private final String name;
  /**
   * list of sclass objects.
   */
  private final List<SClass> classes;
  /**
   * constructor of spackage.
   * @param name of package
   */
  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }
  /**
   * add class to package.
   * @param cls class of package
   */
  public void addClass(SClass cls) {
    classes.add(cls);
  }
  /**
   * classes.
   * @return list of classes
   */
  public List<SClass> classes() {
    return classes;
  }
  /**
   * tostring.
   * @return name of packages
   */
  public String toString() {
    return name;
  }

  @Override
  public String icon() {
    if (classes.isEmpty()) {
      return "./resources/pack_empty_co.gif";
    }
    return "./resources/package_mode.gif";
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public Color background() {
    return null;
  }
}
