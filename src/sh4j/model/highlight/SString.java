package sh4j.model.highlight;

import sh4j.model.style.SStyle;
/**
 * class string.
 * @author Javier
 *
 */
public class SString implements SHighlighter {

  @Override
  public boolean needsHighLight(String text) {
    if (text.length() < 2) {
      return false;
    }
    return text.charAt(0) == '"' && text.charAt(text.length() - 1) == '"';
  }

  @Override
  public String highlight(String text, SStyle style) {
    return style.formatString(text);
  }

}
