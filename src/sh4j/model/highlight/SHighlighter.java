package sh4j.model.highlight;

import sh4j.model.style.SStyle;
/**
 * shighlighter interface.
 * @author Javier
 *
 */
public interface SHighlighter {
  /**
   * needs highlight method.
   * @param text to be highlighted
   * @return boolean true or false
   */
  public boolean needsHighLight(String text);
  /**
   * highlight a text based on style.
   * @param text to be highlighted
   * @param style to be used 
   * @return highlighted text;
   */
  public String highlight(String text, SStyle style);
}
